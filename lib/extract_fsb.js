var fs = require("fs");
var child = require("child_process");

var fsb = {};

fsb.extract = function(_infile, _outdir, _exec)
{
    _exec = _exec || "fsb_aud_extr.exe";

    if(!fs.statSync(_infile).isFile())
    {
        return "Input file " + _infile + " not found!";
    }
    
    if(!fs.statSync(_outdir).isDirectory())
    {
        return _outdir + " is not a directory!";
    }

    if(!fs.statSync(_exec).isFile())
    {
        return _exec + " is not a file!";
    }

    return child.spawn(_exec, [_infile], {
        cwd: _outdir,
    });
};

module.exports = fsb;