var fs = require("fs");

var dtj = {};

var readSection = function(_context)
{
    // The text we actually need
    var sectionText = "";

    // Where the section starts
    var sectStart = _context.content.indexOf(":", _context.offset);
    var sectColon = sectStart;

    // There are no sections
    if(sectStart < 0)
    {
        return false;
    }
    
    // Revert to the start of the section name
    for(; sectStart > 0; sectStart--)
    {
        var c = _context.content.substring(sectStart, sectStart + 1);

        if(!/\S/.test(c))
        {
            break;
        }
    }

    // Where the section ends 
    var sectEnd = _context.content.indexOf(":", sectColon + 1);

    // Revert to the start of the section name
    for(; sectEnd > 0; sectEnd--)
    {
        var c = _context.content.substring(sectEnd, sectEnd + 1);

        if(!/\S/.test(c))
        {
            break;
        }
    }

    // Are we back to the start
    if(sectEnd == 0)
    {
        return false;
    }
    // WTF?
    else if(sectEnd < 0)
    {
        sectEnd = _context.content.length;
    }

    // WTF? No. 2
    if(sectEnd - sectStart < 2)
    {
        return false;
    }

    // Set the section
    sectionText = _context.content.substring(sectStart, sectEnd).trim();

    // Name of this section
    var sectionName = sectionText.split(":")[0].trim();
    // All the values
    var sectionValues = sectionText.split(":")[1].trim();

    // Object to be put into context.sections
    var sectionObj = {
        name: sectionName,
        values: {}
    };

    // Splits (hopefully) only at value start
    var split = sectionValues.split(/\s\./);

    for(var i = 0; i < split.length; i++)
    {
        var valueStr = split[i].trim();

        if(valueStr.length < 1)
        {
            continue;
        }

        // Regex magic to keep quoted strings together
        var valuesSplitRaw = valueStr.split(/((?:"[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|\/[^\/\\]*(?:\\[\S\s][^\/\\]*)*\/[gimy]*(?=\s|$)|(?:\\\s|\S))+)(?=\s|$)/);
        var valuesSplit = [];

        for(var j = 0; j < valuesSplitRaw.length; j++)
        {
            if(valuesSplitRaw[j].trim().length > 0)
            {
                valuesSplit.push(valuesSplitRaw[j]);
            }
        }

        var valueObj = {};

        if(valuesSplit.length < 1)
        {
            continue;
        }

        valueObj.name = valuesSplit[0].trim();
        // Make sure the dot is not included
        if(valueObj.name.startsWith("."))
        {
            valueObj.name = valueObj.name.substring(1);
        }

        valueObj.args = [];

        for(var j = 1; j < valuesSplit.length; j++)
        {
            var arg;
            try
            {
                // If possible get it as JSON to allow for proper numbers
                arg = JSON.parse(valuesSplit[j]);
            }
            catch(e)
            {
                arg = valuesSplit[j].trim();
            }

            valueObj.args.push(arg);
        }

        sectionObj.values[valueObj.name] = valueObj.args.length == 1 ? valueObj.args[0] : valueObj.args;
    }

    _context.sections.push(sectionObj);
    _context.offset = sectEnd;

    return true;
};

dtj.convert = function(_content)
{
    var asString = _content.toString();

    // Just makin' sure
    if(typeof(asString) !== "string")
    {
        return null;
    }

    asString = asString.trim();

    // Check if its empty, needs at least two characters 'NAME:'
    if(asString.length < 2)
    {
        // TODO: Maybe it should return null?
        return {};
    }

    var preprocessed = "";
    var lines = asString.split("\n");

    for(var i = 0; i < lines.length; i++)
    {
        var line = lines[i].trim();

        if(
            line.startsWith("//") ||
            line.length < 1
        )
        {
            continue;
        }

        preprocessed += line + " ";
    }
    
    // What we are parsing
    var context = {
        // The string we are parsing
        content: preprocessed,
        // Where we are in the file
        offset: 0,
        // The sections we have already parsed
        sections: [],
    };

    while(readSection(context));

    return context.sections;
};

dtj.sync = function(_file)
{
    return dtj.convert(fs.readFileSync(_file));
};

dtj.async = function(_file, _callback)
{
    fs.readFile(_file, function(err, data) {
        if(err)
        {
            return _callback(err);
        }

        _callback(err, dtj.convert(data));
    });
};

module.exports = dtj;
