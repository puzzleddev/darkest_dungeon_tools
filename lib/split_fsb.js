var fs = require("fs");

var fsb = {};

var FSB_START_MARK = new Buffer("FSB");

fsb.convert = function(_buffer)
{
    var index = _buffer.indexOf(FSB_START_MARK);

    if(index < 0)
    {
        return null;
    }

    return _buffer.slice(index);
};

fsb.sync = function(_infile, _outfile)
{
    var buffer = fs.readFileSync(_infile, {
        flag: "r",
    });

    fs.writeFileSync(_outfile, fsb.convert(buffer), {
        flag: "w",
    });

    return true;
};

fsb.async = function(_infile, _outfile, _callback)
{
    fs.readFile(_infile, {
        flag: "r",
    }, function(_err, _buffer) {
        if(_err)
        {
            return _callback(_err);
        }

        fs.writeFile(_outfile, fsb.convert(_buffer), {
            flag: "w",
        }, function(err) {
            _callback(err);
        });
    });
};

module.exports = fsb;