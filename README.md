# darkest_dungeon_tools

Various tools to work with files used in the video game 'Darkest Dungeon'.

## darkest_to_json

A tool to parse .darkest files from the game 'Darkest Dungeon' into a sensible JSON interpretation.

### CLI

If no second argument is given it will just append '.json' to the file name.

`$ darkest_tools darkestjson INFILE [OUTFILE]`

### Functions

`darkest_to_json.convert(_content: string) -> Object`

Parses a string containing darkest data.

`darkest_to_json.sync(_file: string / Buffer) -> Object`

Parses a file synchronously.

`darkest_to_json.async(_file: string / Buffer, _callback: (_err, _Object) -> void) -> Object`

Parses a file asynchronously.

## extract_fsb

Retrieves a valid FMOD FSB file from a .bank audio storage file.

### CLI

If no second argument is given it will just append '.fsb' to the file name.

`$ darkest_tools extractfsb INFILE [OUTFILE]`

### Functions

`extract_fsb.convert(_buffer: Buffer) -> Buffer`

Converts the contents of a BANK file into a valid FSB format.

`extract_fsb.sync(_infile: string, _outfile: string) -> void`

Converts a BANK file synchronously.

`extract_fsb.sync(_infile: string, _outfile: string, _callback: (err) -> void) -> void`

Converts a BANK file asynchronously.