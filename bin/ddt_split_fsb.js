#!/usr/bin/env node

var dtj;

try
{
    dtj = require("..").split_fsb;
} catch(e)
{
    
    dtj = require("darkest_dungeon_tools").split_fsb;
}

var fs = require("fs");
var process = require("process");

var main = function(argv)
{
    if(argv < 3)
    {
        return "Too few arguments";
    }
    
    var inFile = argv[2];
    var outFile;
    
    if(argv.length > 3)
    {
        outFile = argv[3];
    }
    else
    {
        outFile = inFile + ".fsb";
    }
    
    dtj.sync(inFile, outFile);
};

module.exports = main;
