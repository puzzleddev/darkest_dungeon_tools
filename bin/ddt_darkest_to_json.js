#!/usr/bin/env node

var dtj;

try
{
    dtj = require("..").darkest_to_json;
} catch(e)
{
    
    dtj = require("darkest_dungeon_tools").darkest_to_json;
}

var fs = require("fs");
var process = require("process");

var main = function(argv)
{
    if(argv.length < 3)
    {
        return "Too few arguments";
    }
    
    var inFile = argv[2];
    var outFile;
    
    if(argv.length > 3)
    {
        outFile = argv[3];
    }
    else
    {
        outFile = inFile + ".json";
    }
    
    fs.writeFileSync(outFile, JSON.stringify(dtj.sync(inFile), null, 2));
};

module.exports = main;