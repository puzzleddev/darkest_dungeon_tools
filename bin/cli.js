#!/usr/bin/env node

/**
 * Script for 'darkest_tools' executable.
 * Essentially a redirect to all the other tools available.
 */

var darkest_to_json = require("./ddt_darkest_to_json");
var split_fsb = require("./ddt_split_fsb");
var extract_fsb = require("./ddt_extract_fsb");

var process = require("process");

var tools = {
    "darkestjson": {
        help: "darkestjson INFILE [OUTFILE]",
        main: darkest_to_json,
    },
    "splitfsb": {
        help: "splitfsb INFILE [OUTFOLDER]",
        main: split_fsb,
    },
    "extractfsb": {
        help: "darkestjson INFILE [OUTFILE] [EXEC_LOCATION]",
        main: extract_fsb,
    },
};

var printHelp = function()
{
    var keys = Object.keys(tools);

    for(var i = 0; i < keys.length; i++)
    {
        var t = tools[keys[i]];

        console.log("  " + keys[i] + " - " + t.help);
    }
};

if(process.argv.length < 3)
{
    console.log("Too few arguments!");
    printHelp();

    return;
}

var tool = tools[process.argv[2]];

if(tool)
{
    var res = tool.main(process.argv.splice(1));

    if(typeof(res) === "string")
    {
        console.log(res);
        console.log(tool.help);
    }

    return;
}

console.log("Tool '" + process.argv[2] + "' not found!");
printHelp();