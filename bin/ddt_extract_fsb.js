#!/usr/bin/env node

var dtj;

try
{
    dtj = require("..").extract_fsb;
} catch(e)
{
    
    dtj = require("darkest_dungeon_tools").extract_fsb;
}

var fs = require("fs");
var process = require("process");

var main = function(argv)
{
    if(argv < 3)
    {
        return "Too few arguments";
    }
    
    var inFile = argv[2];
    var outDir;
    var exec;
    
    if(argv.length > 3)
    {
        outDir = argv[3];
    }
    else
    {
        outDir = inFile + "_out";
    }

    if(argv.length > 4)
    {
        exec = argv[4];
    }   
    else
    {
        exec = "fsb_aud_extr.exe";
    } 

    var val = dtj.extract(inFile, outDir, exec);

    if(typeof(val) === "string")
    {
        console.log(val);
    }
};

module.exports = main;
